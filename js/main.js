var app = new Vue({
	el: '#ejecVue1',
	data: {
		message: 'Haciendo sliders con Vue!',
		arreglos: [
			{
				src: 'http://aacf1985.org/aacf/wp-content/uploads/2016/01/The-Seed.jpg',
				alt: 'alt1',
				title: 'title1'
			},
			{
				src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRnEYZuy1nR4wCtj5w-n4Vu0lHh07N3b6M--YQoFuHOEg7wbL6C',
				alt: 'alt2',
				title: 'title2'
			},
			{
				src: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQY2NPGYjOU4Ws6kfaVEY8seeA1uiPhZVDSYH7RAZklwUpW7Tx9',
				alt: 'alt3',
				title: 'title3'
			},
			{
				src: 'https://media.istockphoto.com/photos/plant-growing-picture-id510222832?k=6&m=510222832&s=612x612&w=0&h=Pzjkj2hf9IZiLAiXcgVE1FbCNFVmKzhdcT98dcHSdSk=',
				alt: 'alt4',
				title: 'title4'
			},
			{
				src: 'http://www.wbfarmstore.net/wp-content/uploads/2015/01/DirtAndShovel.jpg',
				alt: 'alt5',
				title: 'title5'
			},
	    ],
	    timer:undefined,
	},
	mounted: function () {
        this.startRotation();
    },
	
	methods: {
		startRotation: function() {
            this.timer = setInterval(this.next, 3000);
        },

        stopRotation: function() {
            clearTimeout(this.timer);
            this.timer = undefined;
        },

		next: function() {
			var first = this.arreglos.shift();
			this.arreglos.push(first);
			// example1.items.splice(indexOfItem, 1, newValue)

		},
		prev: function() {
			var last = this.arreglos.pop();
			this.arreglos.unshift(last);
			// console.log('Probando ready');
		},
		
	},
});
