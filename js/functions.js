var app = new Vue({
	el: '#ejecVue3',

	data: {
		// message: 'Ejercicio de Vue2!',
		// frutas: ['banana', 'naranja', 'mandarina', 'manzana', 'pera']	
		frutas: [],
		editing: false,
		item_on_edit:{
			image: {}
		},
		timer:undefined,
	},

	created: function () {
		this.loadfrutas();
	},

	mounted: function () {
		this.startRotation();
		this.item_on_edit = this.empty_item();
	},

	computed: {
		is_valid: function(){
			return this.validate();
		}
		
		
	},

	methods: {
		startRotation: function() {
            this.timer = setInterval(this.next, 3000);
        },

        stopRotation: function() {
            clearTimeout(this.timer);
            this.timer = undefined;
        },

		next: function() {
			var first = this.frutas.shift();
			this.frutas.push(first);
			// example1.items.splice(indexOfItem, 1, newValue)

		},
		prev: function() {
			var last = this.frutas.pop();
			this.frutas.unshift(last);
			// console.log('Probando ready');
		},

		loadfrutas: function() {
			console.log('Si esta corriendo load');
			axios.get('./data/frutas.json')
			.then(response => {
				this.frutas = response.data;
			})
			.catch(response => {
				console.log('error');
				// this.frutas = response;
			});
		},

		editar: function(index) {
			if(this.editing){
				console.log('esta editando');
				return;
			}
			this.editing=true;
			this.item_on_edit = this.frutas[index];
			this.borrar(index);
			
		},

		validate: function() {

			if (this.item_on_edit.text === undefined || this.item_on_edit.text.trim() == '' ) {
				return false;
			}

			if (this.item_on_edit.a === undefined || this.item_on_edit.a.trim() == '' || !this.validUrl(this.item_on_edit.a)) {
				return false;
			}

			if (this.item_on_edit.image.src === undefined || this.item_on_edit.image.src.trim() == '' || !this.validUrl(this.item_on_edit.image.src) ) {
				return false;
			}

			if (this.item_on_edit.image.alt === undefined || this.item_on_edit.image.alt.trim() == '' ) {
				return false;
			}

			if (this.item_on_edit.image.title === undefined || this.item_on_edit.image.title.trim() == '' ) {
				return false;
			} 

			return true
				
			
		},

		disable: function() {
			if (this.is_valid === true) {
				console.log('is true');
			}
		},
		

		validUrl: function(url) {
			var regex= /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
			if (!regex.test(url)) {
				console.log('valida url');
				return false;

			}
			return true;
		},

		// validSrc: function(src) {
		// 	var regex= /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/
		// 	if (!regex.test(src)) {
		// 		console.log('valida src');
		// 		return false;

		// 	}
		// 	return true;
		// },

		borrar: function(index) {
			// console.log(index);
			this.$delete(this.frutas, index)
			// this.frutas.splice(index, 1)
		},

		crear: function() {
			this.editing=false;
			this.frutas.push(this.item_on_edit);
			this.item_on_edit = this.empty_item();
		},

		validCreate: function() {
			if (!this.is_valid ) {
				alert('Requiere que llene todos los campos y que ingrese url correcta');
				return;
			} 

			this.crear();
		},

		//Se cambia como funcion para no alterar a item on edit
		empty_item:  function() { 
			return{ text: undefined, 
				a:undefined,
				image: {
					src: undefined,
					alt: undefined,
					title: undefined
				}
			}
		},
	},

});